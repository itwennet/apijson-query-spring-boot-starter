package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;


/**
 * <p>基于Lambda构建查询参数</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/19 18:06
 */

public class ApiJsonQueryLambdaWrapper<T> extends AbstractQueryWrapper<T, ColumnUtil.SFunction<T, ?>, ApiJsonQueryLambdaWrapper<T>> implements ApiJsonQuery {


    @SafeVarargs
    @Override
    public final ApiJsonQueryLambdaWrapper<T> select(ColumnUtil.SFunction<T, ?>... columns) {
        return select(true,columns);
    }

    @SafeVarargs
    @Override
    public final ApiJsonQueryLambdaWrapper<T> select(boolean condition, ColumnUtil.SFunction<T, ?>... columns) {
        if(condition) {
            for (ColumnUtil.SFunction<T, ?> column : columns) {
                columnList.add(ColumnUtil.getName(column));
            }
        }
        return typedThis;
    }

    @Override
    protected ApiJsonQueryLambdaWrapper<T> addCondition(boolean condition, ColumnUtil.SFunction<T, ?> column, EnumKeyword keyword, Object val) {
        if (condition) {
            this.conditionHandle(new Condition(parseName(column), keyword, val));
        }
        return typedThis;
    }

    @Override
    protected ApiJsonQueryLambdaWrapper<T> addAggFunc(boolean condition, ColumnUtil.SFunction<T, ?> column, EnumKeyword keyword, Object val) {
        if (condition) {
            this.aggFuncHandle(keyword, parseName(column), val);
        }
        return typedThis;
    }

    @Override
    protected ApiJsonQueryLambdaWrapper<T> addGroupFunc(boolean condition, ColumnUtil.SFunction<T, ?> column, EnumKeyword keyword) {
        if (condition) {
            this.groupFuncHandle(keyword,parseName(column));
        }
        return typedThis;
    }

    @Override
    protected ApiJsonQueryLambdaWrapper<T> addOrderByFunc(boolean condition, ColumnUtil.SFunction<T, ?> column, EnumKeyword keyword, String s) {
        if (condition) {
            this.orderByFuncHandle(keyword,parseName(column) + s);
        }
        return typedThis;
    }

    @Override
    protected ApiJsonQueryLambdaWrapper<T> instance() {
        return new ApiJsonQueryLambdaWrapper<>();
    }

    private String parseName(ColumnUtil.SFunction<T, ?> column) {
        return null == column ? null : ColumnUtil.getName(column);
    }

}
