package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import java.util.Arrays;

/**
 * <p>基于String形式构建查询参数</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/19 21:40
 */
public class ApiJsonQueryStringWrapper<T> extends AbstractQueryWrapper<T, String, ApiJsonQueryStringWrapper<T>> implements ApiJsonQuery {


    @Override
    protected ApiJsonQueryStringWrapper<T> addCondition(boolean condition, String column, EnumKeyword keyword, Object val) {
        if (condition) {
            //条件
            this.conditionHandle(new Condition(column, keyword, val));
        }
        return typedThis;
    }


    @Override
    protected ApiJsonQueryStringWrapper<T> addAggFunc(boolean condition, String column, EnumKeyword keyword, Object val) {
        if (condition) {
            this.aggFuncHandle(keyword, column, val);
        }
        return typedThis;
    }

    @Override
    protected ApiJsonQueryStringWrapper<T> addGroupFunc(boolean condition, String column, EnumKeyword keyword) {
        if (condition && keyword.equals(EnumKeyword.GROUP)) {
            this.groupFuncHandle(keyword, column);
        }
        return typedThis;
    }

    @Override
    protected ApiJsonQueryStringWrapper<T> addOrderByFunc(boolean condition, String column, EnumKeyword keyword, String s) {
        if (condition && keyword.equals(EnumKeyword.ORDER_BY)) {
            this.orderByFuncHandle(keyword, column + s);
        }
        return typedThis;
    }

    @Override
    protected ApiJsonQueryStringWrapper<T> instance() {
        return new ApiJsonQueryStringWrapper<>();
    }

    @Override
    public ApiJsonQueryStringWrapper<T> select(String... columns) {
        return select(true, columns);
    }

    @Override
    public ApiJsonQueryStringWrapper<T> select(boolean condition, String... columns) {
        if (condition) {
            columnList.addAll(Arrays.asList(columns));
        }
        return typedThis;
    }

}

