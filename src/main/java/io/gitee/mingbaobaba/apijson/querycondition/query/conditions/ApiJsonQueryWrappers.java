package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

/**
 * <p>条件构造</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/22 22:17
 */
public class ApiJsonQueryWrappers {

    private ApiJsonQueryWrappers() {
    }

    /**
     * 构建基于String方式的查询条件实例
     *
     * @param <T> 泛型
     * @return DwQueryStringWrapper
     */
    public static <T> ApiJsonQueryStringWrapper<T> query() {
        return new ApiJsonQueryStringWrapper<>();
    }

    /**
     * 构建基于map方式的查询条件实例 该参数支持apiJson原生写法
     *
     * @param <T> 泛型
     * @return DwQueryMapWrapper
     */
    public static <T> ApiJsonQueryMapWrapper<T> mapQuery() {
        return new ApiJsonQueryMapWrapper<>();
    }

    /**
     * 构建基于Lambda方式的查询条件实例
     *
     * @param <T> 泛型
     * @return DwQueryLambdaWrapper
     */
    public static <T> ApiJsonQueryLambdaWrapper<T> lambdaQuery() {
        return new ApiJsonQueryLambdaWrapper<>();
    }
}
