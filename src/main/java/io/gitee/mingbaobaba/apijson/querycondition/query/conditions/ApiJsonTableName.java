package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import java.lang.annotation.*;

/**
 * <p>表名</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/21 11:57
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiJsonTableName {

    /**
     * 表名
     */
    String value() default "";

    /**
     * schema
     */
    String schema() default "";

    /**
     * 业务标识
     */
    String biSigns() default "";

    /**
     * 描述
     */
    String desc() default "";

}
