package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>apiJson关键词</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/9 13:33
 */
@AllArgsConstructor
@Getter
public enum EnumApiJson {

    COLUMN("@column"),

    COMBINE("@combine"),

    GROUP("@group"),

    HAVING("@having"),

    ORDER("@order"),

    EXPLAIN("@explain");

    private final String code;

}
