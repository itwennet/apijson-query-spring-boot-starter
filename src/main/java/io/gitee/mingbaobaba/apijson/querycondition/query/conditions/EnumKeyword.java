package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import lombok.AllArgsConstructor;

/**
 * <p>连接条件枚举</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/19 21:22
 */
@AllArgsConstructor
public enum EnumKeyword {
    /**
     * 连接条件枚举
     */
    AND,
    OR,
    NOT,
    NULL,
    NOT_NULL,
    IN,
    NOT_IN,
    LIKE,
    NOT_LIKE,
    EQ,
    NE,
    GT,
    GE,
    LT,
    LE,
    REG_EXP,
    BETWEEN_AND,

    /**
     * 函数
     */
    COUNT,
    SUM,
    MAX,
    MIN,
    AVG,
    GROUP,
    /**
     * 排序
     */
    ORDER_BY;
}
