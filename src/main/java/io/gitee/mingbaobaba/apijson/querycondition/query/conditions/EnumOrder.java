package io.gitee.mingbaobaba.apijson.querycondition.query.conditions;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * <p>排序</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/9 13:45
 */
@AllArgsConstructor
@Getter
public enum EnumOrder {

    DESC("-"),

    ASC("+");

    private final String code;
}
