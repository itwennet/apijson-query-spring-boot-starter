package io.gitee.mingbaobaba.apijson.querycondition.query.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>数仓服务配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/20 21:46
 */
@Data
@Component
@ConfigurationProperties(prefix = "datawarehouse")
public class ApiJsonQueryProperties {

    /**
     * 客户端ID
     */
    private String clientId;

    /**
     * 客户端密钥
     */
    private String clientSecret;

    /**
     * 授权类型
     */
    private String grantType = "client_credentials";

    /**
     * 授权范围
     */
    private String scope = "all";

    /**
     * 请求token地址
     */
    private String tokenUrl = "ttp://localhost:8080/oauth/token";

    /**
     * 服务调用地址
     */
    private String serviceUrl = "http://localhost:8080/get";

}
